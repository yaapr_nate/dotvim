call pathogen#incubate()
call pathogen#helptags()

""Set go plugins for vim
filetype plugin indent on
set rtp+=$GOROOT/misc/vim
syntax on

""set statusline=%<\ %n:%f\ %m%r%y%=%-35.(line:\ %l\ of\ %L,\ col:\ %c%V\ (%P)%)
let g:airline#extensions#tabline#enabled = 1

autocmd Filetype ruby setlocal ts=2 sw=2 expandtab

set hidden
set number
set ts=4 sts=4 sw=4 expandtab
set hlsearch
set showmatch
set incsearch
set autoindent
set history=1000

""Nerd Tree setupt
autocmd VimEnter * NERDTree
autocmd VimEnter * wincmd p
let NERDTreeShowBookmarks=1
let NERDTreeChDirMode=0
let NERDTreeQuitOnOpen=0
let NERDTreeMouseMode=2
let NERDTreeIgnore=['\.pyc','\~$','\.swo$','\.swp$','\.git','\.hg','\.svn','\.bzr']
let NERDTreeKeepTreeInNewTab=1
let g:nerdtree_tabs_open_on_gui_startup=0
set t_Co=256

let g:airline_theme='dark'

" Gif config
map  / <Plug>(easymotion-sn)
omap / <Plug>(easymotion-tn)

" These `n` & `N` mappings are options. You do not have to map `n` & `N` to
" EasyMotion.
" Without these mappings, `n` & `N` works fine. (These mappings just provide
" different highlight method and have some other features )
map n <Plug>(easymotion-next)
map N <Plug>(easymotion-prev)

colorscheme molokai
syntax on


autocmd VimEnter * nested :TagbarOpen
