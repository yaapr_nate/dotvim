#!/usr/bin/env bash

echo "Setting up VIM environment"
user=$(whoami)
user_home_directory=$(awk -F: -v v="$user" '{if ($1==v) print $6}' /etc/passwd)

user_vim_directory="$user_home_directory/.vim"
user_vim_backup_directory="$user_vim_directory.backup"
if [ -f $user_vim_directory ];
then
	echo "Found existing $user_vim_directory.  Making backup directory at $user_vim_backup_directory"
	mv $user_vim_directory $user_vim_backup_directory
fi

user_vimrc_file="$user_home_directory/.vimrc"
user_vimrc_backup_file="$user_vimrc_file.backup"
if [ -f $user_vimrc_file ];
then
	echo "Found existing $user_vimrc_file. Making backup file at $user_vimrc_backup_file"
	mv $user_vimrc_file $user_vimrc_backup_file
fi

current_full_path=$(pwd)
echo "Creating symlink from $current_full_path to $user_vim_directory"
ln -s $current_full_path $user_vim_directory

current_vimrc_full_path=$current_full_path/vimrc
echo "Creating symlink from $current_vimrc_full_path $user_vimrc_file"
ln -s $current_vimrc_full_path $user_vimrc_file

echo "Setting up git submodule"
/usr/bin/git submodule init
/usr/bin/git submodule update
